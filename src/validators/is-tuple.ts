import {NoEarlyExitProps, ValidationError, ValidationFunction} from '../interfaces';
import {FORMAT_MISPATCH} from '../error-keys';
import {isArray} from './is-array';

export function isTuple<TProps extends NoEarlyExitProps>(elements: ValidationFunction<TProps>[]): ValidationFunction<TProps>
{
    const preconditionValidator = isArray();
    
    return async (value, props) => {
        const preconditionErrors = preconditionValidator(value, props);
        if (preconditionErrors)
            return preconditionErrors;

        const noEarlyExit = props.noEarlyExit;
        const errors: ValidationError[] = [];
        
        if (elements.length < value.length)
        {
            errors.push({
                errorKey: FORMAT_MISPATCH,
                details: {
                    expectedMaxLength: elements.length,
                    length: value.length
                }
            });
            
            if (!noEarlyExit)
                return errors;
        }
        
        for (let index = 0; index < elements.length; index++)
        {
            const currentItem = value[index];
            
            const currentErrors = await elements[index](currentItem, props);

            if (currentErrors)
            {
                for (const error of currentErrors)
                {
                    (error.path || (error.path = [])).unshift(index);
                }

                errors.push(...currentErrors);

                if (!noEarlyExit)
                    break;
            }
        }

        return errors.length ? errors : null;
    };
}