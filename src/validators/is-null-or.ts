import {anyOf} from './any-of';
import {isNull} from './is-null';
import {ValidationFunction} from '../interfaces';

export function isNullOr<TProps>(validator: ValidationFunction<TProps>): ValidationFunction<TProps>
{
    return anyOf([
        isNull(),
        validator
    ]);
}