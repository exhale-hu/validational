import {ValidationFunction} from '../interfaces';
import {isString} from './is-string';
import {validate} from 'email-validator';
import {FORMAT_MISPATCH} from '../error-keys';

export function isEmail<TProps>(): ValidationFunction<TProps>
{
    const preconditionCheck = isString();
    
    return (value, props) => {
        const preconditionErrors = preconditionCheck(value, props);
        if (preconditionErrors)
            return preconditionErrors;
        
        return validate(value) ? null : [{
            errorKey: FORMAT_MISPATCH,
            details: {
                expected: 'email',
                value
            }
        }];
    };
}