import {ValidationFunction} from '../interfaces';
import {BAD_TYPE} from '../error-keys';

export function isTypeOf<TProps>(type: string): ValidationFunction<TProps>
{
    return value => {
        const actualType = typeof value;

        return actualType === type ? null : [{
            errorKey: BAD_TYPE,
            details: {
                expected: type,
                got: actualType,
                value
            }
        }];
    };
}