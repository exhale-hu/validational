import {ValidationFunction} from '../interfaces';
import {BAD_TYPE} from '../error-keys';

export function isArray<TProps>(): ValidationFunction<TProps>
{
    return value => Array.isArray(value) ? null : [{
        errorKey: BAD_TYPE,
        details: {
            expected: 'array',
            got: typeof value,
            value
        }
    }];
}