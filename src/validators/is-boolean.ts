import {ValidationFunction} from '../interfaces';
import {isTypeOf} from './is-type-of';

export function isBoolean<TProps>(): ValidationFunction<TProps>
{
    return isTypeOf('boolean');
}