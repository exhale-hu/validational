import {ValidationFunction} from '../interfaces';
import {isString} from './is-string';
import isStringUrl from 'is-url';
import {FORMAT_MISPATCH} from '../error-keys';

export function isUrl<TProps>(): ValidationFunction<TProps>
{
    const preconditionCheck = isString();
    
    return (value, props) => {
        const preconditionErrors = preconditionCheck(value, props);
        if (preconditionErrors)
            return preconditionErrors;
        
        return isStringUrl(value) ? null : [{
            errorKey: FORMAT_MISPATCH,
            details: {
                expected: 'url',
                value
            }
        }];
    };
}