import {NoEarlyExitProps, ValidationError, ValidationFunction} from '../interfaces';
import {isObject} from './is-object';
import {ADDITIONAL_PROPERTY, MISSING_PROPERTY} from '../error-keys';

export function hasProperties<TProps extends NoEarlyExitProps>(properties: { [key: string]: ValidationFunction<TProps> }, options?: {
    additionalProperties?: boolean|((props: TProps) => boolean),
    allPropertiesRequired?: boolean|((props: TProps) => boolean)
}): ValidationFunction<TProps>
{
    const objectValidator = isObject();

    return async (value, props) => {
        const preconditionErrors = objectValidator(value, props);
        if (preconditionErrors)
            return preconditionErrors;

        const noEarlyExit = props.noEarlyExit;

        const errors: ValidationError[] = [];

        for (const [key, validator] of Object.entries(properties))
        {
            if (!(key in value))
            {
                if (!options
                    || undefined === options.allPropertiesRequired
                    || true === options.allPropertiesRequired
                    || (typeof options.allPropertiesRequired === 'function'
                        && false !== options.allPropertiesRequired(props)
                    )
                )
                {
                    errors.push({
                        errorKey: MISSING_PROPERTY,
                        path: [key]
                    });
                    continue;
                }
            }

            const currentErrors = await validator(value[key], props);

            if (currentErrors)
            {
                for (const error of currentErrors)
                {
                    (error.path || (error.path = [])).unshift(key);
                }

                errors.push(...currentErrors);

                if (!noEarlyExit)
                    break;
            }
        }

        if ((noEarlyExit || !errors.length) && options
            && (false === options.additionalProperties
                || (typeof options.additionalProperties === 'function'
                    && !options.additionalProperties(props)))
        )
        {
            for (const key of Object.keys(value))
            {
                if (properties[key])
                    continue;

                errors.push({
                    errorKey: ADDITIONAL_PROPERTY,
                    path: [key]
                });

                if (!noEarlyExit)
                    break;
            }
        }

        return errors;
    };
}