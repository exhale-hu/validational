import {ValidationError, ValidationFunction} from '../interfaces';

export function anyOf<TProps>(validators: ValidationFunction<TProps>[]): ValidationFunction<TProps>
{
    return async (value, props) => {
        const errors: ValidationError[] = [];

        let success = false;
        for (const validator of validators)
        {
            const error = await validator(value, props);

            if (error)
            {
                errors.push(...error);
            }
            else
            {
                success = true;
                break;
            }
        }

        return success ? null : errors;
    };
}