import {ValidationFunction} from '../interfaces';
import {BAD_TYPE} from '../error-keys';

export function isObject<TProps>(): ValidationFunction<TProps>
{
    return value => {
        const actualType = typeof value;

        return actualType === 'object' && null !== value ? null : [{
            errorKey: BAD_TYPE,
            details: {
                expected: 'object',
                got: null === value ? 'null' : actualType,
                value
            }
        }];
    };
}