import {NoEarlyExitProps, ValidationError, ValidationFunction} from '../interfaces';
import {isObject} from './is-object';
import {ADDITIONAL_PROPERTY} from '../error-keys';

export function doesNotHaveProperties<TProps extends NoEarlyExitProps>(properties: string[]): ValidationFunction<TProps>
{
    const objectValidator = isObject();

    return async (value, props) => {
        const preconditionErrors = objectValidator(value, props);
        if (preconditionErrors)
            return preconditionErrors;

        const noEarlyExit = props.noEarlyExit;

        const errors: ValidationError[] = [];

        for (const key of properties)
        {
            if (key in value)
            {
                errors.push({
                    errorKey: ADDITIONAL_PROPERTY,
                    path: [key]
                });

                if (!noEarlyExit)
                    break;
            }
        }

        return errors;
    };
}