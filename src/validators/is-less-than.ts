import {ValidationFunction} from '../interfaces';
import {OUT_OF_RANGE} from '../error-keys';

export function isLessThan<TProps, T>(lessThan: T): ValidationFunction<TProps>
{
    return value => value < lessThan ? null : [{
        errorKey: OUT_OF_RANGE,
        expected: { lessThan },
        value
    }];
}